function ajaxHelper(uri, method, data) {
    
    return $.ajax({
            url: uri,
            type: method,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function(jqXHR, textStatus, errorThrown) {
            console.log('uri ' + uri);
            console.log('method' + method);
            console.log(errorThrown);
        });

}


var ViewModel = function() {

}

$(document).ready(function() {
    var viewModel = new ViewModel();

    ko.applyBindings(viewModel);

});

