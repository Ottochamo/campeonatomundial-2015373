var express = require('express');
var Autenticacion = require('../helper/autenticacion');

var router = express.Router();
var auth = Autenticacion();

/* GET home page. */
router.get('/', function(req, res, next) {
  //passport libreria

  auth.autorizar(req);
  res.render(auth.getPath() + '/index');

});

router.get('/autenticar', function(req, res, next) {
  res.render('default/autenticar');  
});

router.get('/registrar', function(req, res, next) {
  res.render('default/registrar');
});

router.get('/cookies', function(req, res, next) {
  res.status(200).send(req.cookies);
});

router.get('/clear', function(req, res) {
  res.clearCookie('nick');
  res.clearCookie('idUsuario');
  res.status(200).send('cookies eliminadas');
});

router.get('/cerrar', function(req, res, next) {
  res.clearCookie('idUsuario');
  res.clearCookie('nick');
  res.redirect('/');
});

router.get('/categorias', function(req, res, next) {
  res.render('categoria');
});

router.get('/prueba', function(req, res, next) {
  res.status(200).send('HOLA PRUEBA');
});

module.exports = router;
