var express = require('express');
var usuario = require('../model/usuario');

var router = express.Router();


router.get('/api/Usuario:idUsuario', function(req, res) {
    var idUsuario = req.params.idUsuario;

    usuario.getUsuario(idUsuario, function(error, resultados) {
        if (typeof resultados !== undefined) {
            res.json(resultados);
        } else {
            res.json({'Mensaje': 'No se encontró al usuario'});
        }
    });
});

router.post("/autenticar", function(req, res) {
    var data = {
        "nick": req.body.nick,
        "contrasena": req.body.contrasena
    };

    usuario.autenticar(data, function(error, resultado) {
        if (resultado !== undefined) {
            res.cookie('idUsuario', resultado[0].idUsuario);
            res.cookie('nick', resultado[0].nick);
            console.log('Se guardo la cookie');

            res.redirect('/');
        } else {
            res.json({'Mensaje': 'No se ingreso el Usuario'});
        }
    });
});

router.post('/registrar', function(req, res) {
    var data = {
        'idUsuario': null,
        'nick': req.body.nick,
        'contrasena': req.body.contrasena
    };

    usuario.insert(data, function(error, resultado) {
        if(resultado.insertId > 0) {
            res.redirect('/');
        } else {
            res.json({'Mensaje': 'No se pudo registrar'});
        }
    });

});

router.put('/api/Usuario/:idUsuario', function(req, res) {
    var data = {
        'idUsuario': req.body.idUsuario,
        'nick': req.body.nick,
        'contrasena': req.body.contrasena
    };

    usuario.update(data, function(error, resultado) {
        if (resultado.insertId > 0 ) {
            res.redirect('/');
        } else {
            res.json({'Mensaje': 'No se pudo editar el usuario'});
        }
    });

});

router.delete('/api/Usuario/:idUsuario', function(req, res) {
    var idUsuario = req.params.idUsuario;

    usuario.delete(idUsuario, function(error, result) {
        if (result) {
            res.redirect('/api/Usuario/');
        } else {
            res.json({'Mensaje': 'No se pudo eliminar'});
        }
    });

});

module.exports = router;