var database = require('./database/connection');

var usuario = {};

usuario.getUsuarios = function(callback) {
    if (database) {
        database.query('SELECT * FROM Usuario', 
        function(error, results) {
            if (error) {
                throw error;
            } else {
                return results;
            }
        });
    }
}

usuario.getUsuario = function(idUsuario, callback) {
    if (database) {
        var sql = 'SELECT * FROM Usuario WHERE idUsuario = ?';
        database.query(sql, idUsuario, 
        function(error, result) {
            if (error) {
                throw error;
            } else {
                callback(null, result);
            }
        });
    }
}

usuario.insert = function(data, callback) {
    if (database) {
        var sql = 'call sp_InsertarUsuario(?, ?, ?)';
        var values = [[data.idPais, data.nick, data.contrasena]]
        database.query(sql, [values], 
        function(error, result) {
            if (error) {
                callback(error, null);
            } else {
                callback(null, {'insertId': result.insertId});
            }
        });
    }
}


usuario.autenticar = function(data,  callback) {
    if (database) {
        var sql = 'SELECT * FROM Usuario WHERE nick = ? AND contrasena = ?';
        database.query(sql, [data.nick, data.contrasena], 
        function(error, result) {
            if (error) {
                throw error;
            } else {
                callback(null, result);
            }
        });
    }
}

module.exports = usuario;