CREATE DATABASE CampeonatoMundial;

USE CampeonatoMundial;

CREATE TABLE Pais (
	idPais INT NOT NULL AUTO_INCREMENT,
	nombre VARCHAR(100) NOT NULL,
    CONSTRAINT pk_pais PRIMARY KEY(idPais)
);

CREATE TABLE Usuario (
	idUsuario INT NOT NULL AUTO_INCREMENT,
	idPais INT NOT NULL,
    nick VARCHAR(50) NOT NULL,
    contrasena VARCHAR(50) NOT NULL,
    CONSTRAINT pk_usuario PRIMARY KEY (idUsuario),
    CONSTRAINT fk_usuariopais FOREIGN KEY (idPais) REFERENCES Pais(idPais),
    CONSTRAINT unique_idPais UNIQUE (idPais, nick)
);


CREATE TABLE Versus (
	idVersus INT NOT NULL AUTO_INCREMENT,
	idRival INT NOT NULL,
    idContricante INT NOT NULL,
    CONSTRAINT pk_Versus PRIMARY KEY (idVersus),
	CONSTRAINT fk_idrival FOREIGN KEY (idRival) 
		REFERENCES Usuario (idUsuario),
    CONSTRAINT fk_idcontricante FOREIGN KEY (idContricante) 
		REFERENCES Usuario (idUsuario),
	Fecha DATETIME DEFAULT NOW()
);

DROP TABLE Pais;
DROP TABLE Versus;
DROP TABLE Usuario;


